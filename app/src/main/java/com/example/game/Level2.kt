package com.example.game

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.view.MotionEvent
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.universal.*
import org.w3c.dom.Text
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class Level2 : AppCompatActivity() {

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) hideSystemUI()
    }

    private fun hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                // Set the content to appear under the system bars so that the
                // content doesn't resize when the system bars hide and show.
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Hide the nav bar and status bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }

    // Shows the system bars by removing all the flags
// except for the ones that make the content appear under the system bars.
    private fun showSystemUI() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.universal)
        var numLeft: Int //leftnumber photo + text
        var numRight: Int //rightnumer photo + text
        val arrayoffiles = arrayoffiles() //array of images/texts
        var random = Random()
        var count = 0
        var dialogEnd:Dialog//


        //Dialog start
        var dialog: Dialog
        dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.previewdialog)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)

        val btnclose = dialog.findViewById(R.id.btnclose) as TextView

        btnclose.setOnClickListener(){
            val intent = Intent(this, levelsactivity::class.java)
            startActivity(intent)
            finish()
            dialog.dismiss()

        }

        var btncontinue = dialog.findViewById(R.id.btncontinue) as Button
        btncontinue.setOnClickListener(){
            dialog.dismiss()

        }

        dialog.show()
        //dialog end

        dialog.show()
        //dialog end


        //dialogEnd
        dialogEnd = Dialog(this)
        dialogEnd.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogEnd.setContentView(R.layout.dialogend)
        dialogEnd.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogEnd.setCancelable(false)

        var btncontinue2 = dialogEnd.findViewById(R.id.btncontinue) as Button
        btncontinue2.setOnClickListener(){

            val intent = Intent(this@Level2, Level3::class.java)
            startActivity(intent)
            finish()

            dialogEnd.dismiss()

        }
        var btnmenu = dialogEnd.findViewById(R.id.btnmenu) as Button
        btnmenu.setOnClickListener(){
            val intent = Intent(this@Level2,levelsactivity::class.java)
            startActivity(intent)
            finish()

        }


        //dialogEnd






        btn_back.setOnClickListener(){
            val intent = Intent(this, levelsactivity::class.java)
            startActivity(intent)
            finish()
        }

         //ANIMATION CONNECTING
        val a = AnimationUtils.loadAnimation(this@Level2, R.anim.alpha)
        //ANIMATION

        //Progress
        val progress = intArrayOf(
            R.id.point1,
            R.id.point2,
            R.id.point3,
            R.id.point4,
            R.id.point5,
            R.id.point6,
            R.id.point7,
            R.id.point8,
            R.id.point9,
            R.id.point10,
            R.id.point11,
            R.id.point12,
            R.id.point13,
            R.id.point14,
            R.id.point15,
            R.id.point16,
            R.id.point17,
            R.id.point18,
            R.id.point19,
            R.id.point20
        )

        //Progress


        //random numbers start

        numLeft = random.nextInt(10)
        img_left.setImageResource(arrayoffiles.images2[numLeft]) //random number generator
        text_left.setText(arrayoffiles.texts2[numLeft])

        numRight = random.nextInt(10)

           //to prevent same numbers
        while (numLeft == numRight){
            numRight = random.nextInt(10)

            //random numbers end

        }
        img_right.setImageResource(arrayoffiles.images2[numRight])
        img_right.startAnimation(a)
        text_right.setText(arrayoffiles.texts2[numRight])
        //randend2



           //leftclick start
        img_left.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                img_right.isEnabled = false
                if (numLeft > numRight) {
                    img_left.setImageResource(R.drawable.correct_answer)
                } else {
                    img_left.setImageResource(R.drawable.false_answer)
                }
            } else if (event.action == MotionEvent.ACTION_UP) {
                //if left is bigger
                if (numLeft > numRight) {
                    if (count < 20) {
                        count = count + 1
                    }
                    //coloring progress
                    for (i in 0..19) {
                        val tv = findViewById<TextView>(progress[i])
                        tv.setBackgroundResource(R.drawable.style_points_gray)
                    }

                    for (i in 0 until count) {
                            val tv = findViewById<TextView>(progress[i])
                            tv.setBackgroundResource(R.drawable.style_points_green)
                            //green
                    }

                } else {
                    //if left is lower
                    if (count > 0) {
                        if (count == 1) {
                            count = 0

                        } else {
                            count = count - 2
                        }

                    }
                    for (i in 0..18) {
                        val tv = findViewById<TextView>(progress[i])
                        tv.setBackgroundResource(R.drawable.style_points_gray)


                    }

                    for (i in 0 until count) {
                        val tv = findViewById<TextView>(progress[i])
                        tv.setBackgroundResource(R.drawable.style_points_green)
                        //green


                    }
                }

                    if (count ==20){

                        dialogEnd.show()


                    }else{
                        //random numbers start

                        numLeft = random.nextInt(10)
                        img_left.setImageResource(arrayoffiles.images2[numLeft]) //random number generator
                        img_left.startAnimation(a   )
                        text_left.setText(arrayoffiles.texts2[numLeft])


                        numRight = random.nextInt(10)

                        //to prevent same numbers
                        while (numLeft == numRight){
                            numRight = random.nextInt(10)

                            //random numbers end

                        }
                        img_right.setImageResource(arrayoffiles.images2[numRight])
                        img_right.startAnimation(a)
                        text_right.setText(arrayoffiles.texts2[numRight])
                        //randend2

                        img_right.setEnabled(true) //turning on rightclick
                }


            }
            true
        }

        //rightclick start
        img_right.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                img_left.isEnabled = false
                if (numLeft < numRight) {
                    img_right.setImageResource(R.drawable.correct_answer)
                } else {
                    img_right.setImageResource(R.drawable.false_answer)
                }
            } else if (event.action == MotionEvent.ACTION_UP) {
                //if left is bigger
                if (numLeft < numRight) {
                    if (count < 20) {
                        count = count + 1
                    }
                    //coloring progress
                    for (i in 0..19) {
                        val tv = findViewById<TextView>(progress[i])
                        tv.setBackgroundResource(R.drawable.style_points_gray)
                    }

                    for (i in 0 until count) {
                        val tv = findViewById<TextView>(progress[i])
                        tv.setBackgroundResource(R.drawable.style_points_green)
                        //green
                    }
                } else {
                    //if left is lower
                    if (count > 0) {
                        if (count == 1) {
                            count = 0

                        } else {
                            count = count - 2
                        }

                    }
                    for (i in 0..18) {
                        val tv = findViewById<TextView>(progress[i])
                        tv.setBackgroundResource(R.drawable.style_points_gray)


                    }

                    for (i in 0 until count) {
                        val tv = findViewById<TextView>(progress[i])
                        tv.setBackgroundResource(R.drawable.style_points_green)
                        //green


                    }
                }

                if (count ==20){

                    dialogEnd.show()


                }else{
                    //random numbers start

                    numLeft = random.nextInt(10)
                    img_left.setImageResource(arrayoffiles.images2[numLeft]) //random number generator
                    img_left.startAnimation(a   )
                    text_left.setText(arrayoffiles.texts2[numLeft])


                    numRight = random.nextInt(10)

                    //to prevent same numbers
                    while (numLeft == numRight){
                        numRight = random.nextInt(10)

                        //random numbers end

                    }
                    img_right.setImageResource(arrayoffiles.images2[numRight])
                    img_right.startAnimation(a)
                    text_right.setText(arrayoffiles.texts2[numRight])
                    //randend2

                    img_left.setEnabled(true) //turning on rightclick



                }


            }
            true
        }


    }



}


